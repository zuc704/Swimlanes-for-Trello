function lineBreak(){
    var columnNames = document.getElementsByClassName("list-header-name-assist js-list-name-assist");
    
    //support up to 4 rows.  There's always a first row.
    var row2 = 0;
    var row3 = 0;
    var row4 = 0;
    
    //check for swimlane indicator "{int}" and set number of rows accordingly
    for (var i=0; i < columnNames.length; i++){
        if(columnNames[i].innerText.includes("{2-")){
            row2 = row2 + 1;
            columnNames[i].parentElement.parentElement.parentElement.id = "row2-" + row2;
        }
        if(columnNames[i].innerText.includes("{3-")){
            row3 = row3 + 1;
            columnNames[i].parentElement.parentElement.parentElement.id = "row3-" + row3;
        }
        if(columnNames[i].innerText.includes("{4-")){
            row4 = row4 + 1;
            columnNames[i].parentElement.parentElement.parentElement.id = "row4-" + row4;
        }
        }

//create an element id for the board canvas for each row we find
    document.getElementsByClassName("board-canvas")[0].id = "boardCanvas1";


if(row2 > 0){
    var boardCanvas2 = document.createElement("div");
    boardCanvas2.className = "board-canvas";
    boardCanvas2.setAttribute("id","boardCanvas2");

    document.getElementsByClassName("board-main-content")[0].appendChild(boardCanvas2);
    var board2 = document.createElement("div");
    board2.className = "u-fancy-scrollbar js-no-higher-edits js-list-sortable ui-sortable";
    board2.setAttribute("id","board2");
    document.getElementById("boardCanvas2").appendChild(board2);

}
if(row3 > 0){
    var boardCanvas3 = document.createElement("div");
    boardCanvas3.className = "board-canvas";
    boardCanvas3.setAttribute("id","boardCanvas3");

    document.getElementsByClassName("board-main-content")[0].appendChild(boardCanvas3);
    var board3 = document.createElement("div");
    board3.className = "u-fancy-scrollbar js-no-higher-edits js-list-sortable ui-sortable";
    board3.setAttribute("id","board3");
    document.getElementById("boardCanvas3").appendChild(board3);
}
if(row4 > 0){
    var boardCanvas4 = document.createElement("div");
    boardCanvas4.className = "board-canvas";
    boardCanvas4.setAttribute("id","boardCanvas4");

    document.getElementsByClassName("board-main-content")[0].appendChild(boardCanvas4);
    var board4 = document.createElement("div");
    board4.className = "u-fancy-scrollbar js-no-higher-edits js-list-sortable ui-sortable";
    board4.setAttribute("id","board4");
    document.getElementById("boardCanvas4").appendChild(board4);
}


//adjust canvas heights
if(row2 > 0 && row3 > 0 && row4 > 0){
    for(var r4=0; r4 < 4; r4++){
        document.getElementsByClassName("board-canvas")[r4].style.maxHeight = "25%";
        document.getElementsByClassName("board-canvas")[r4].style.minHeight = "300px";
    }
}
else if(row2 > 0 && row3 > 0){
    for(var r3=0;r3<3;r3++){
        document.getElementsByClassName("board-canvas")[r3].style.maxHeight = "33%";
        document.getElementsByClassName("board-canvas")[r3].style.minHeight = "300px";
    }
}
else if(row2 > 0){
    for (var r2 = 0;r2 < 2;r2++){
        document.getElementsByClassName("board-canvas")[r2].style.maxHeight = "50%";
        document.getElementsByClassName("board-canvas")[r2].style.minHeight = "300px";
    }
}

//foreach row 2 column (we already have fixed number)
for(var r2c = 0; r2c < row2;r2c++){
var board1Columns = document.getElementById("board").childNodes;
for(var col = 0; col < board1Columns.length - 1;col++){
    if(board1Columns[col].firstChild.firstChild.childNodes[1].innerText.includes("{2-" + (r2c + 1) +"}")){
        document.getElementById("board2").appendChild(board1Columns[col]);
    }

}
}
for(var r3c = 0; r3c < row3;r3c++){
var board1Columns = document.getElementById("board").childNodes;
for(var col = 0; col < board1Columns.length - 1;col++){
    if(board1Columns[col].firstChild.firstChild.childNodes[1].innerText.includes("{3-" + (r3c + 1) +"}")){
        document.getElementById("board3").appendChild(board1Columns[col]);
    }

}
}
for(var r4c = 0; r4c < row4;r4c++){
var board1Columns = document.getElementById("board").childNodes;
for(var col = 0; col < board1Columns.length - 1;col++){
    if(board1Columns[col].firstChild.firstChild.childNodes[1].innerText.includes("{4-" + (r4c + 1) +"}")){
        document.getElementById("board4").appendChild(board1Columns[col]);
    }

}
}


//finally renaming #board to #board1 so that we don't have the weird drag-drop issue(javascript method fires which moves everything back to first row)
document.getElementById("board").id = "board1";


$('head').append('<link rel="stylesheet" href="styles.css" type="text/css"');
}


lineBreak();